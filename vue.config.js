// vue.config.js
module.exports = {
    // 关闭eslint 语法检查
    lintOnSave: process.env.NODE_ENV !== 'production',
}